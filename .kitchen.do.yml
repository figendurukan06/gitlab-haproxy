---
driver:
  name: digitalocean
  private_networking: false
  ipv6: false
  region: <%= ENV['GITLAB_DO_REGION'] || "nyc3" %>

provisioner:
  name: chef_zero
  data_bags_path: "test/integration/data_bags"
  require_chef_omnibus: "12"

platforms:
  - name: ubuntu-16-04-x64
    driver_config:
      server_name: <%= ENV['USER'] || 'iamgroot' %>-kitchen-test-1604.template.gitlab-cookbooks.ci.gitlab.test

verifier:
  name: inspec

transport:
  name: rsync
  ssh_key: <%= ENV['GITLAB_DO_SSH_KEY'] || "~/.ssh/id_rsa" %>
  username: root

suites:
  - name: haproxy-frontend
    run_list:
      - recipe[gitlab-haproxy::frontend]
    verifier:
      inspec_tests:
        - test/integration/default
        - test/integration/frontend
    attributes:
      gitlab-haproxy:
        api_address: 0.0.0.0  # for integration tests
        chef_vault: secrets
        chef_vault_item: certs
        frontend:
          api:
            servers:
              api01.stg.gitlab.com: 127.0.0.1
          https_git:
            servers:
              git01.stg.gitlab.com: 127.0.0.1
          registry:
            servers:
              registry01.be.gitlab.com: 127.0.0.1
          ssh:
            port: 2222
            servers:
              git01.stg.gitlab.com: 127.0.0.1
          web:
            servers:
              web01.stg.gitlab.com: 127.0.0.1
          websockets:
            servers:
              web01.stg.gitlab.com: 127.0.0.1

  - name: haproxy-frontend-cloudflare
    run_list:
      - recipe[gitlab-haproxy::frontend]
    verifier:
      inspec_tests:
        - test/integration/default
        - test/integration/frontend
        - test/integration/cloudflare
    attributes:
      gitlab-haproxy:
        api_address: 0.0.0.0  # for integration tests
        chef_vault: secrets
        chef_vault_item: certs
        cloudflare:
          enable: true
        frontend:
          api_rate_limit:
            enforced: false
          api:
            servers:
              api01.stg.gitlab.com: 127.0.0.1
          https_git:
            servers:
              git01.stg.gitlab.com: 127.0.0.1
          registry:
            servers:
              registry01.be.gitlab.com: 127.0.0.1
          ssh:
            port: 2222
            servers:
              git01.stg.gitlab.com: 127.0.0.1
          web:
            servers:
              web01.stg.gitlab.com: 127.0.0.1
          websockets:
            servers:
              web01.stg.gitlab.com: 127.0.0.1

  - name: haproxy-pages
    run_list:
      - recipe[gitlab-haproxy::pages]
    verifier:
      inspec_tests:
        - test/integration/default
        - test/integration/pages
    attributes:
      gitlab-haproxy:
        chef_vault: secrets
        chef_vault_item: certs
        pages:
          servers:
            web01.stg.gitlab.com: 127.0.0.1

  - name: haproxy-altssh
    run_list:
      - recipe[gitlab-haproxy::altssh]
    verifier:
      inspec_tests:
        - test/integration/default
        - test/integration/altssh
    attributes:
      gitlab-haproxy:
        chef_vault: secrets
        chef_vault_item: certs
        altssh:
          servers:
            ssh01.stg.gitlab.com: 127.0.0.1

  - name: haproxy-registry
    run_list:
      - recipe[gitlab-haproxy::registry]
    verifier:
      inspec_tests:
        - test/integration/default
    attributes:
      gitlab-haproxy:
        chef_vault: secrets
        chef_vault_item: certs
        registry:
          servers:
            registry01.be.gitlab.com: 127.0.0.1

  - name: haproxy-camoproxy
    run_list:
      - recipe[gitlab-haproxy::camoproxy]
    verifier:
      inspec_tests:
        - test/integration/default
        - test/integration/camoproxy
    attributes:
      gitlab-haproxy:
        chef_vault: secrets
        chef_vault_item: certs
